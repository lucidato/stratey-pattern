package com.strategy.patternstrategy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatternstrategyApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatternstrategyApplication.class, args);
	}

}
